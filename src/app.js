const puppeteer = require('puppeteer');
puppeteer.launch({
    headless: false,
    args: [
        '--no-sandbox',
        //-------> Un comment below line line if security is no issue and to clear all cross site errors 
        // '--disable-web-security',
    ]
}).then(async browser => {
    const page = await browser.newPage();
    await page.goto('https://www.ticketmaster.com/event/3B0054CDD5532C65?bba=1');
    await page.waitFor(1 * 5000);
    await page.click('#modal-dialog > div > div.modal-dialog.modal-dialog--center.modal-dialog-ccp-v2.landing-modal__modal > div.modal-dialog__content.modal-dialog__content--fullscreen > div > div.landing-modal-footer > button.button-aux.modal-dialog__button.landing-modal-footer__see-tickets-button');
    await page.waitFor(1 * 5000);
    await page.click('#quickpicks-listings > ul > li:nth-child(8)');
    await page.waitFor(1 * 5000);
    await page.click('#offer-card-buy-button');
    await page.waitFor(1 * 5000);
    await page.click('#cta_event_footer');
    const myframe = (await page.frames())[1];
});