# README #

To Use Run below in Root Directory.
- npm install
- node src/app.js

### Remarks ###
* Delay of 5 second is used instead of [ await page.waitForNavigation(); ] to simulate requests like real user as ticket master tracks time between requests.
* Script is working of below tasks-
   * Opens chromium browser with URL of predefined event
   * Selects best 2 tickets.
   * confirms the tickets
   * Go to Login Page after confirming the tickets 
   * Loads the login page i-frame
   *  Only Filling and submission  of login page is pending due to some chrome security issues as login page is loaded from different domain in i-frame which is against chrome rules. To reslove this need to disable chrome security parameters.           
